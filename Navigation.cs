using Priority_Queue;

public struct NavigableConnection
{
    public INavigable nextNode;
    public float cost;
}

public interface INavigable
{
    public IEnumerable<NavigableConnection> GetNavigableConnections();
}


// Dijkstra's pathfinding through a graph of connected INavigable nodes.
// requires Priority_Queue from https://github.com/BlueRaja/High-Speed-Priority-Queue-for-C-Sharp,
//    also in the ThirdParty folder
public static class Navigation
{
    //TODO: Write tests
    //TODO: add heuristic function
    public static List<INavigable> GetPath(INavigable fromNode, INavigable targetNode)
    {
        if (fromNode == targetNode)
        {
            return new List<INavigable>(){ targetNode };
        }

        SimplePriorityQueue<MazeNode, float> fringe = new SimplePriorityQueue<MazeNode, float>();
        Dictionary<INavigable, INavigable> previousNode = new Dictionary<INavigable, INavigable>();
        Dictionary<INavigable, float> nodeCosts = new Dictionary<INavigable, float>();

        fringe.Enqueue(fromNode, 0);
        nodeCosts[fromNode] = 0f;

        INavigable headNode = fromNode;
        
        while (!headNode == targetNode)
        {
            foreach(INavigableConnection connection in headNode.GetNavigableConnections())
            {
                if (!nodeCosts.ContainsKey(connection.nextNode)) // filters out previously explored
                {
                    float nodeCost = nodeCosts[headNode] + connection.cost;
                    nodeCosts[connection.nextNode] = nodeCost;
                    previousNode[connection.node] = headNode;
                    fringe.Enqueue(connection.node, nodeCost);
                }
            }

            headNode = fringe.Dequeue();                
        }
        // headNode is now our targetNode, so we're done pathfinding
        // just need to reassemble the path
        List<INavigable> result = new List<INavigable>();
        while (headNode != fromNode)
        {
            result.Add(headNode);
            headNode = previousNode[headNode];
        }

        return result;
    }


}
